<?php

class Plugins_Auth_Acl extends Zend_Controller_Plugin_Abstract 
{

    public function preDispatch(\Zend_Controller_Request_Abstract $request)
    {
        $auth = Zend_Auth::getInstance();
        $acl = Zend_Registry::get('acl');
        //se o usuário não autenticou o seu papel é guest
        if (!$auth->hasIdentity())
        {
            $user = new Plugins_Auth_UserAcl();
            $user->setRole('guest');
            $auth->getStorage()->write($user);
        }

        $role = $auth->getStorage()->read()->getRole();

        parent::preDispatch($request);
        
        $module     = $request->getModuleName();
        $controller = $request->getControllerName();
        $action     = $request->getActionName();
        
        $resource = "{$module}:{$controller}:{$action}";

        if( $resource == 'panel:index:index' && $acl->isAllowed($role, 'panel:index:dashboard', 'dashboard') ) {
            $request->setModuleName('panel');
            $request->setControllerName('index');
            $request->setActionName('dashboard');
        }

        if ( !$acl->isAllowed($role, $resource, $action) ) {
            $request->setModuleName('main');
            $request->setControllerName('error');
            $request->setActionName('denied');
        }

    }
}
