<?php

class Plugins_Auth_AuthSetup
{

    public static function login($username, $password)
    {
        $db = Zend_Registry::get('db');

        $adapter = new Zend_Auth_Adapter_DbTable($db);
        $adapter->setTableName('User')
                ->setIdentityColumn('username')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('sha1(?)');

        $adapter->getDbSelect()->where("status = 's'");

        $adapter->setIdentity($username)
                ->setCredential($password);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        $user = new Plugins_Auth_UserAcl();

        if ($result->isValid())
        {
            $data = $adapter->getResultRowObject(NULL, 'password');
            $user->setRole($data->role);
            $user->setUsername($data->username);
            $user->setName($data->name);
            $user->setEmail($data->email);
            $auth->getStorage()->write($user);
            return true;
        }else
            return false;
    }

}