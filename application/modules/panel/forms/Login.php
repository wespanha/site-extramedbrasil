<?php

class Panel_Form_Login extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$username = new Zend_Form_Element_Text('username');
		$username->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control',
				 					'placeholder' => 'Nome de usuário',
				 					'ng-model' => 'login.username',
				 					'required' => ''));
		$this->addElement($username);

		$password = new Zend_Form_Element_Password('password');
		$password->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control',
				 					'placeholder' => 'Senha',
				 					'ng-model' => 'login.password',
				 					'required' => ''));
		$this->addElement($password);

	}

}