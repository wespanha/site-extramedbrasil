<?php

class Main_Form_Contato extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$nome = new Zend_Form_Element_Text('nome');
		$nome->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'Nome Completo',
				 					'required' => ''));
		$this->addElement($nome);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'E-mail',
				 					'required' => ''));
		$this->addElement($email);

		$mensagem = new Zend_Form_Element_Textarea('mensagem');
		$mensagem->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'Mensagem',
				 					'required' => '',
				 					'rows' => 6));
		$this->addElement($mensagem);

	}

}