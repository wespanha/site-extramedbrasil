<?php

class Main_IndexController extends Zend_Controller_Action
{

	private $form_contato;

    public function init()
    {
    	$this->form_contato = new Main_Form_Contato();    
    }

    public function indexAction()
    {    

    	if( $this->getRequest()->getParam('msg') ) {
    		$this->view->assign('mensagem', 'Obrigado por entrar em contato. Sua mensagem foi enviada para os nossos atendentes e em breve iremos responder.');
    	}	

    	if( $this->getRequest()->isPost() ) {
			$form_data = $this->getRequest()->getPost();
			
			$config = array('ssl' => 'ssl',
			                'auth' => 'login',
			                'username' => 'contato@extramedbrasil.com.br',
			                'port' => '465',
			                'password' => 'lt6pfja2T65h');

			$transport = new Zend_Mail_Transport_Smtp('mx1.hostinger.com.br', $config);

			$html = "<p>Nome: ".$form_data['nome']."</p>";
			$html .= "<p>Email: ".$form_data['email']."</p>";
			$html .= "<p>Mensagem: ".$form_data['mensagem']."</p>";

			$mail = new Zend_Mail('utf8');
			$mail->setBodyHtml($html);
			$mail->setFrom('contato@extramedbrasil.com.br', 'Contato Site Extramed Brasil');
			$mail->addTo('diretoria@extramedbrasil.com.br');
			$mail->setSubject('Contato através do site Extramed Brasil');

			if( $mail->send($transport) ) {
				$this->redirect('/index/index/msg/success/#contato');
			}
		}

    	$this->view->assign('form', $this->form_contato);
	    
    }

}

