<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * Inicializa o banco de dados. Somente necessario se deseja salvar a conexao no registro
     * @name _initDB
     * @return void
     */
    public function _initDB() {
        $db = $this->getPluginResource('db')->getDbAdapter();
        Zend_Db_Table::setDefaultAdapter($db);
        Zend_Registry::set('db', $db);
    }

    /**
     * Inicializa o Namespace do plugin Acl
     * @name _initAutoLoader
     * @return void
     */
    protected function _initAutoloader(){
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Plugins');
    }

    /**
     * Inicializa o Plugin App/Plugin/Acl.php
     * @name _initPlugins
     * @return void
     */
    protected function _initPlugins(){
        $bootstrap = $this->getApplication();
        if ($bootstrap instanceof Zend_Application) {
            $bootstrap = $this;
        }
        $bootstrap->bootstrap('FrontController');
        $front = $bootstrap->getResource('FrontController');

        $front->registerPlugin(new Plugins_Auth_Acl());

    }

    /**
     * Função para carregar automaticamente o diretorio de layout's para cada modulo
     * @name _initLayout
     * @return void
     */
    public function _initLayout() {
        $request_uri = explode('/', $_SERVER['REQUEST_URI']);
        $modules = array('main','panel');
        $layout = "main";

        foreach($modules as $key => $value) {
            (in_array($value, $request_uri)) ? $layout = $value : '';
        }

        $option = array(
            'layout'        => 'layout',
            'layoutPath' => APPLICATION_PATH . "/modules/". $layout . "/layouts"
        );

        Zend_Layout::startMvc($option);
    }
    
    /**
     * Função responsável por conectar a api do sistema
     * @name _initConexaoWs
     * @return void
     */
    public function _initConexaoWs() {
        try {

            //instacia o serviço http
            $http = new Zend_Http_Client();

            $http->setHeaders('Accept','application/json;odata=verbose');
            $http->setHeaders('Content-Type','application/json; charset=utf8');
            
            // Registra o webservices
            $registry = Zend_Registry::getInstance();
            $registry->set('http', $http);
        }
        catch( Zend_Exception $e) {
            echo "Estamos sem conexão ao banco de dados no momento. Por favor, tente mais tarde.";
        }
    }

    /**
     * Inicializa com banco de dados Sql
     * @name _initDatabase
     * @return void
     */
    protected function _initDatabase()
    {
        //@var $resource Zend_Application_Resource_Multidb
        /*$resource = $this->getPluginResource('multidb');*/
        // inicia as conexões aos bancos de dados
        /*$resource->init();
        Zend_Registry::set('mssql', $resource->getDb('mssql'));*/
    }

    
    public function _initAcl()
    {
        $acl = new Zend_Acl();
        $acl->addRole('guest')
            ->addRole('administrator', 'guest');
            
        $acl->addResource('main:index:index');
        
        $acl->addResource('main:error:error');
        $acl->addResource('main:error:denied');

        $acl->addResource('panel:index:index');
        $acl->addResource('panel:index:dashboard');
        $acl->addResource('panel:index:logout');
        
        $acl->addResource('panel:error:error');

        $acl->addResource('panel:dashboard:index');

        $acl->allow('guest', 'main:index:index');

        $acl->allow('guest', 'main:error:error');
        $acl->allow('guest', 'main:error:denied');

        $acl->allow('guest', 'panel:index:index');
        $acl->allow('guest', 'panel:index:logout');

        $acl->allow('guest', 'panel:error:error');

        $acl->allow('administrator', 'panel:index:dashboard');

        $acl->allow('administrator', 'panel:dashboard:index');
        
        Zend_Registry::set('acl', $acl);
    }
}

